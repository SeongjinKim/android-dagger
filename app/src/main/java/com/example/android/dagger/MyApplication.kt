/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.dagger

import android.app.Application
import com.example.android.dagger.storage.SharedPreferencesStorage
import com.example.android.dagger.user.UserManager
import dagger.hilt.android.HiltAndroidApp

// 1. userManager 외에 다른 매니저가 계속 생기면 어떻게 될까? (DirectoryManager, PreferenceManager, NetworkManager 등등)
// 2. 싱글톤 객체 말고 매번 다른 객체를 받고 싶다면 어떻게 할까?
// 3. 특정 상황에서만 동일한 객체를 사용하고, 그 외에는 폐기한 후 새로운 객체를 생성하고 싶다면 어떡할까 (login, logout)

@HiltAndroidApp
open class MyApplication : Application() {
    open val userManager by lazy {
        UserManager(SharedPreferencesStorage(this))
    }
}
