package com.example.android.dagger.di

import com.example.android.dagger.storage.SharedPreferencesStorage
import com.example.android.dagger.storage.Storage
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

// Dagger Module임을 알린다.
// Dagger가 직접 생성할 수 없는 객체일 경우는 Model 에서 제공받아 공급한다.
// 예) 인터페이스 구현체, 레트로핏 서비스 등
// @Binds 어노테이션으로 인해 추상클래스로 선언한다,
// Hilt에 의해 생성된 ApplicationComponent에 모듈을 설치한다
@InstallIn(ApplicationComponent::class)
@Module
abstract class StorageModule {

    // 반환 타입은 공급하길 원하는 인터페이스 구현체이다.
    // 파아미터로 공급하길 원하는 구현체를 특정지을 수 있다.
    // SharedPreferenceStorage 를 Storage의 구현체로 공급한다.
    @Binds
    abstract fun provideStorage(storage: SharedPreferencesStorage): Storage
}